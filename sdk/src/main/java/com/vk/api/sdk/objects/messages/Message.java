package com.vk.api.sdk.objects.messages;

import com.google.gson.annotations.SerializedName;
import com.vk.api.sdk.objects.base.BoolInt;
import com.vk.api.sdk.objects.base.Geo;

import java.util.List;
import java.util.Objects;

/**
 * Message object
 */
public class Message {
    /**
     * Message ID
     */

    @SerializedName("conversation_message_id")
    private Integer conversationMessageId;

    @SerializedName("is_hidden")
    private Boolean isHidden;

    @SerializedName("id")
    private Integer id;

    @SerializedName("peer_id")
    private Integer peerId;

    /**
     * Date when the message has been sent in Unixtime
     */
    @SerializedName("date")
    private Integer date;

    /**
     * Date when the message has been updated in Unixtime
     */
    @SerializedName("update_time")
    private Integer updateTime;

    /**
     * Information whether the message is outcoming
     */
    @SerializedName("out")
    private BoolInt out;

    /**
     * Message author's ID
     */
    @SerializedName("user_id")
    private Integer userId;

    /**
     * Message author's ID
     */
    @SerializedName("from_id")
    private Integer fromId;

    /**
     * ID used for sending messages. It returned only for outgoing messages
     */
    @SerializedName("random_id")
    private Integer randomId;

    /**
     * Is it an important message
     */
    @SerializedName("important")
    private Boolean important;

    /**
     * Is it an deleted message
     */
    @SerializedName("deleted")
    private BoolInt deleted;

    /**
     * Whether the message contains smiles
     */
    @SerializedName("emoji")
    private BoolInt emoji;

    /**
     * Forwarded messages
     */
    @SerializedName("fwd_messages")
    private List<Message> fwdMessages;

    /**
     * Information whether the messages is read
     */
    @SerializedName("read_state")
    private BoolInt readState;

    /**
     * Message title or chat title
     */
    @SerializedName("title")
    private String title;

    /**
     * Message text
     */
    @SerializedName("text")
    private String body;

    @SerializedName("attachments")
    private List<MessageAttachment> attachments;

    /**
     * Chat ID
     */
    @SerializedName("chat_id")
    private Integer chatId;

    @SerializedName("chat_active")
    private List<Integer> chatActive;

    /**
     * Push settings for the chat
     */
    @SerializedName("push_settings")
    private ChatPushSettings pushSettings;

    /**
     * Action type
     */
    @SerializedName("action")
    private Action action;

    /**
     * User or email ID has been invited to the chat or kicked from the chat
     */
    @SerializedName("action_mid")
    private Integer actionMid;

    /**
     * Email has been invited or kicked
     */
    @SerializedName("action_email")
    private String actionEmail;

    /**
     * Action text
     */
    @SerializedName("action_text")
    private String actionText;

    /**
     * Chat users number
     */
    @SerializedName("users_count")
    private Integer usersCount;

    /**
     * Chat administrator ID
     */
    @SerializedName("admin_id")
    private Integer adminId;

    /**
     * URL of the preview image with 50px in width
     */
    @SerializedName("photo_50")
    private String photo50;

    /**
     * URL of the preview image with 100px in width
     */
    @SerializedName("photo_100")
    private String photo100;

    /**
     * URL of the preview image with 200px in width
     */
    @SerializedName("photo_200")
    private String photo200;

    @SerializedName("geo")
    private Geo geo;

    public Integer getId() {
        return id;
    }

    public Integer getDate() {
        return date;
    }

    public boolean isOut() {
        return out == BoolInt.YES;
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getFromId() {
        return fromId;
    }

    public Integer getRandomId() {
        return randomId;
    }

    public Boolean getImportant() {
        return important;
    }

    public boolean getDeleted() {
        return deleted == BoolInt.YES;
    }

    public boolean getEmoji() {
        return emoji == BoolInt.YES;
    }

    public List<Message> getFwdMessages() {
        return fwdMessages;
    }

    public boolean isReadState() {
        return readState == BoolInt.YES;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public List<MessageAttachment> getAttachments() {
        return attachments;
    }

    public Integer getChatId() {
        return chatId;
    }

    public List<Integer> getChatActive() {
        return chatActive;
    }

    public ChatPushSettings getPushSettings() {
        return pushSettings;
    }

    public Action getAction() {
        return action;
    }

    public Integer getActionMid() {
        return actionMid;
    }

    public String getActionEmail() {
        return actionEmail;
    }

    public String getActionText() {
        return actionText;
    }

    public Integer getUsersCount() {
        return usersCount;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public String getPhoto50() {
        return photo50;
    }

    public String getPhoto100() {
        return photo100;
    }

    public String getPhoto200() {
        return photo200;
    }

    public Geo getGeo() {
        return geo;
    }

    public Integer getConversationMessageId() {
        return conversationMessageId;
    }

    public Boolean getHidden() {
        return isHidden;
    }

    public Integer getPeerId() {
        return peerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(conversationMessageId, message.conversationMessageId) &&
                Objects.equals(isHidden, message.isHidden) &&
                Objects.equals(id, message.id) &&
                Objects.equals(peerId, message.peerId) &&
                Objects.equals(date, message.date) &&
                Objects.equals(updateTime, message.updateTime) &&
                out == message.out &&
                Objects.equals(userId, message.userId) &&
                Objects.equals(fromId, message.fromId) &&
                Objects.equals(randomId, message.randomId) &&
                Objects.equals(important, message.important) &&
                deleted == message.deleted &&
                emoji == message.emoji &&
                Objects.equals(fwdMessages, message.fwdMessages) &&
                readState == message.readState &&
                Objects.equals(title, message.title) &&
                Objects.equals(body, message.body) &&
                Objects.equals(attachments, message.attachments) &&
                Objects.equals(chatId, message.chatId) &&
                Objects.equals(chatActive, message.chatActive) &&
                Objects.equals(pushSettings, message.pushSettings) &&
                action == message.action &&
                Objects.equals(actionMid, message.actionMid) &&
                Objects.equals(actionEmail, message.actionEmail) &&
                Objects.equals(actionText, message.actionText) &&
                Objects.equals(usersCount, message.usersCount) &&
                Objects.equals(adminId, message.adminId) &&
                Objects.equals(photo50, message.photo50) &&
                Objects.equals(photo100, message.photo100) &&
                Objects.equals(photo200, message.photo200) &&
                Objects.equals(geo, message.geo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(conversationMessageId, isHidden, id, peerId, date, updateTime, out, userId, fromId, randomId, important, deleted, emoji, fwdMessages, readState, title, body, attachments, chatId, chatActive, pushSettings, action, actionMid, actionEmail, actionText, usersCount, adminId, photo50, photo100, photo200, geo);
    }

    @Override
    public String toString() {
        return "Message{" +
                "conversationMessageId=" + conversationMessageId +
                ", isHidden=" + isHidden +
                ", id=" + id +
                ", peer_id=" + peerId +
                ", date=" + date +
                ", updateTime=" + updateTime +
                ", out=" + out +
                ", userId=" + userId +
                ", fromId=" + fromId +
                ", randomId=" + randomId +
                ", important=" + important +
                ", deleted=" + deleted +
                ", emoji=" + emoji +
                ", fwdMessages=" + fwdMessages +
                ", readState=" + readState +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", attachments=" + attachments +
                ", chatId=" + chatId +
                ", chatActive=" + chatActive +
                ", pushSettings=" + pushSettings +
                ", action=" + action +
                ", actionMid=" + actionMid +
                ", actionEmail='" + actionEmail + '\'' +
                ", actionText='" + actionText + '\'' +
                ", usersCount=" + usersCount +
                ", adminId=" + adminId +
                ", photo50='" + photo50 + '\'' +
                ", photo100='" + photo100 + '\'' +
                ", photo200='" + photo200 + '\'' +
                ", geo=" + geo +
                '}';
    }
}
